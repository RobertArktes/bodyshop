package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/RobertArktes/bodyshop/internal/handler"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

//@title Bodyshop service Api
//@version 1.0
//@description Api server for bosyshop application

//@host localhost:8082/bodyshop
//@BasePath /
func main() {
	logger := initLogger()

	defer func() {
		if err := logger.Sync(); err != nil {
			zap.L().Error("error syncing logger", zap.Error(err))
		}
	}()

	if err := InitConfig(); err != nil {
		zap.L().Error("error initializing config: %s", zap.Error(err))
	}

	zap.L().Info("config initialized")

	handlers := handler.NewHandler()

	srv := new(Server)

	go func() {
		if err := srv.Run(viper.GetString("server.port"), handlers.InitRoutes()); err != nil &&
			err != http.ErrServerClosed {
			zap.L().Error("error running server: %s", zap.Error(err))
		}
	}()

	zap.L().Info("server started")

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	<-quit

	if err := srv.Shutdown(context.Background()); err != nil {
		zap.L().Error("error shutting down server: %s", zap.Error(err))
	}

	zap.L().Info("server shut down")
}

func InitConfig() error {
	viper.AddConfigPath("cmd/")
	viper.SetConfigName("config")

	return viper.ReadInConfig()
}

type Server struct {
	httpServer *http.Server
}

func (s *Server) Run(port string, handler http.Handler) error {
	s.httpServer = &http.Server{
		Addr:           ":" + port,
		Handler:        handler,
		MaxHeaderBytes: 1 << 20,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   20 * time.Second,
	}

	return s.httpServer.ListenAndServe()
}

func (s *Server) Shutdown(ctx context.Context) error {
	return s.httpServer.Shutdown(ctx)
}

func initLogger() *zap.Logger {
	var logger *zap.Logger

	var err error

	zapConfig := zap.NewProductionConfig()
	zapConfig.EncoderConfig.TimeKey = "timestamp"
	zapConfig.EncoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder

	if logger, err = zapConfig.Build(); err != nil {
		log.Fatal("Error building zap logger:", err.Error())
	}

	zap.ReplaceGlobals(logger)

	return logger
}
