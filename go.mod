module gitlab.com/RobertArktes/bodyshop

go 1.16

require (
	github.com/go-chi/chi/v5 v5.0.7
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.10.1
	github.com/swaggo/http-swagger v1.2.0
	github.com/swaggo/swag v1.7.8
	go.uber.org/zap v1.20.0
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd // indirect
	golang.org/x/sys v0.0.0-20220128215802-99c3d69c2c27 // indirect
	golang.org/x/tools v0.1.9 // indirect
)
