package handler

import (
	"bytes"
	"encoding/json"
	"html/template"
	"net/http"

	"github.com/spf13/viper"
	"gitlab.com/RobertArktes/bodyshop/internal/models"
	"go.uber.org/zap"
)

//@Summary Send mail
//@Tags mail
//@Description Send form and queue
//@ID form-and-queue
//@Param mail body models.Mail true "Form and Queue"
//@Accept json
//@Produce json
//@Failure 400,404 {string} string "bad request"
//@Failure 500 {string} string "server error"
//@Failure default {string} string "other problem"
//@Router /bodushop/form_and_queue [post]
func (h *Handler) FormAndQueue(w http.ResponseWriter, r *http.Request) {
	var NewMail models.Mail

	var tmpl *template.Template

	err := json.NewDecoder(r.Body).Decode(&NewMail)
	if err != nil {
		zap.L().Error("Failed to encode", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed create form"))

		return
	}

	if tmpl, err = template.New("maildata").Parse(*NewMail.Template.TemplateText); err != nil {
		zap.L().Error("Failed to parse template", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to create form"))

		return
	}

	var parsedTemplate *bytes.Buffer

	if err = tmpl.Execute(parsedTemplate, NewMail.Parameters); err != nil {
		zap.L().Error("Failed to execute template", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to create form"))

		return
	}

	var data []byte
	if data, err = json.Marshal(models.FormedMail{
		MailID:      NewMail.MailID,
		UserID:      NewMail.UserID,
		ReceiversID: NewMail.ReceiversID,
		Text:        parsedTemplate.String(),
	}); err != nil {
		zap.L().Error("Failed to marshal form", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to create form"))

		return
	}

	req, err := http.NewRequestWithContext(r.Context(), "POST", viper.GetString("sender.address"), bytes.NewBuffer(data))
	if err != nil {
		zap.L().Error("Failed to create request", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to send form"))

		return
	}

	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		zap.L().Error("Failed to do request", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to get send form"))

		return
	}

	if err = resp.Body.Close(); err != nil {
		zap.L().Error("Failed to close body", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to get send form"))

		return
	}

	w.WriteHeader(http.StatusOK)
}
