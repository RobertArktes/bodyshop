package handler

import (
	"github.com/go-chi/chi/v5"
	httpSwagger "github.com/swaggo/http-swagger"
	_ "gitlab.com/RobertArktes/bodyshop/docs"
)

type Handler struct{}

func NewHandler() *Handler {
	return &Handler{}
}

func (h *Handler) InitRoutes() *chi.Mux {
	router := chi.NewRouter()
	router.Route("/bodyshop", func(r chi.Router) {
		r.Post("/form_and_queue", h.FormAndQueue)

		r.Get("/swagger/*", httpSwagger.Handler(
			httpSwagger.URL("http://localhost:8082/bodyshop/swagger/doc.json")))

	})

	return router
}
