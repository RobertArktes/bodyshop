package handler_test

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi/v5"
	"github.com/stretchr/testify/assert"
	handler_package "gitlab.com/RobertArktes/bodyshop/internal/handler"
	"go.uber.org/zap"
)

func TestHandler_login(t *testing.T) {
	testCases := []struct {
		name               string
		body               string
		expectedStatusCode int
	}{
		{
			name:               "correct variant",
			body:               `{"user_id":"","receivers_id":"","mail_id":"","template":{"metadata":{"file_name":"","id,omitempty":"","parameters":["",""]},"template_text":""},"parameters":{"param1":"val1","param2":"val2"}}`,
			expectedStatusCode: 200,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			handler := handler_package.NewHandler()

			r := chi.NewRouter()
			r.Post("/form_and_queue", handler.FormAndQueue)

			w := httptest.NewRecorder()
			req, err := http.NewRequest("POST", "/form_and_queue", nil)
			if err != nil {
				zap.L().Error("Failed to create request", zap.Error(err))
			}
			req.Header.Set("Content-Type", "application/json")

			r.ServeHTTP(w, req)

			assert.Equal(t, tc.expectedStatusCode, w.Code)
		})
	}
}
